"use strict";
// дарма я темну тему саме для цього макету вибрав , але зробив як міг)))
const body = document.querySelector("body");
const changeThemeBtn = document.createElement("button");
changeThemeBtn.innerText = "Toggle theme";
body.style.backgroundColor = `${localStorage.getItem("theme")}`;
const link = document.createElement("link");
link.rel = "stylesheet";
link.href = `./style/styleDark.css`;
body.insertAdjacentElement("afterbegin", changeThemeBtn);
document.addEventListener("DOMContentLoaded", function () {
  if (localStorage.getItem("theme") === "black") {
    document.head.append(link);
  }
});
changeThemeBtn.addEventListener("click", function () {
  if (localStorage.getItem("theme") === "black") {
    localStorage.setItem("theme", "white");
    body.style.backgroundColor = "white";
    link.remove();
  } else {
    localStorage.setItem("theme", "black");
    body.style.backgroundColor = "black";
    document.head.append(link);
  }
});
